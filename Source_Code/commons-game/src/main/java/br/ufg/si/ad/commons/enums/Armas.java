package br.ufg.si.ad.commons.enums;

public enum Armas {

	REVOLVER(1, "Revolver"),
	FACA(2, "Faca"),
	ESCOPETA(3, "Escopeta"),
	PA(4, "Pá"),
	BOMBA(5, "Bomba"),
	VENENO(6, "Veneno"),
	TACO_BASEBOL(7, "Bastão de Basebol"),
	ESTILINGUE(8, "Estilingue / Atiradeira"),
	ADAGA(9, "Adaga"),
	GARRAFA(10, "Garrafa de vinho"),
	CORDA(11, "Corda");

	private int codigo;
	private String nome;

	Armas(int codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public static Armas getArmaPorCodigo(int codigo) {
		for(Armas armasTemp : Armas.values()) {
			if(armasTemp.getCodigo() == codigo) {
				return armasTemp;
			}
		}
		return null;
	}
}
