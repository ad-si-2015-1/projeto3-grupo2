package br.ufg.si.ad.commons.enums;

public enum Locais {

	SALA_ESTAR(1, "Sala de estar"),
	JARDIM(2, "Jardim da casa"),
	COZINHA(3, "Cozinha"),
	BIBLIOTECA(4, "Biblioteca"),
	QUARTO(5, "Quarto"),
	SALAO_FESTAS(6, "Salao de festas"),
	HALL(7, "Hall"),
	SALA_JANTAR(8, "Sala de jantar");

	private int codigo;
	private String nome;

	Locais(int codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}

	public static Locais getLocalPorCodigo(int codigo) {
		for(Locais localTemp : Locais.values()) {
			if(localTemp.getCodigo() == codigo) {
				return localTemp;
			}
		}
		return null;
	}

}
