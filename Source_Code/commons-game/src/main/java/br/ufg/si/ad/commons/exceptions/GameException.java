package br.ufg.si.ad.commons.exceptions;
/**
 * @author bruno nogueira
 *
 * Classe de excecao do topo da hierarquia de excecoes retornadas na execucao do jogo
 *
 */
public class GameException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 7065155198248991456L;

	/** Construtor padrao da exception GameException */
	public GameException() {
		super();
	}

	/** Construtor da exception GameException
	 *
	 * @param message String contendo descricao da exception*/
	public GameException(String message) {
		super(message);
	}

	/** Construtor da exception GameException que encapsula uma excecao generica
	 *
	 * @param e Excecao generica lancada em algum ponto da regra de negocio da aplicacao*/
	public GameException(Exception e) {
		super(e);
	}

}
