package br.ufg.si.ad.commons.exceptions;
/**
 * @author bruno nogueira
 *
 * Classe de excecoes da regra de negocio do jogador
 */
public class JogadorException extends GameException {

	/**
	 *
	 */
	private static final long serialVersionUID = -1331680557974410421L;

	/**
	 * Construtor padrao da exception JogadorException
	 * */
	public JogadorException() {
		super();
	}

	/**
	 * Construtor da exception JogadorException que passa uma mensagem sobre a excecao
	 *
	 * @param message mensagem com a causa da excecao*/
	public JogadorException(String message) {
		super(message);
	}

	/**
	 * Construtor da exception JogadorException que encapsula uma excecao generica da aplicacao
	 *
	 * @param e Excecao lancada em alguma parte da aplicacao
	 */
	public JogadorException(GameException e) {
		super(e);
	}
}
