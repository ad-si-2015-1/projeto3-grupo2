package br.ufg.si.ad.commons.exceptions;

/**
 * @author bruno nogueira
 *
 * Classe que lanca excecoes referente as regras de negocio do jogo
 */
public class JogoException extends GameException {

	/**
	 *
	 */
	private static final long serialVersionUID = -5682308830533904436L;

	/**
	 * Construtor padrao da exception JogoException;
	 */
	public JogoException() {
		super();
	}

	/**
	 * Construtor da exception JogoException que recebe uma mensagem com descricao da excecao lancada
	 *
	 * @param message contendo a causa da excecao
	 */
	public JogoException(String message) {
		super(message);
	}

	/**
	 * Construtor da exception que encapsula uma excecao generica lancada pela regra de negocio do jogo
	 *
	 * @param e excecao lancada pela regra de negocio do jogo
	 */
	public JogoException(GameException e) {
		super(e);
	}

}
