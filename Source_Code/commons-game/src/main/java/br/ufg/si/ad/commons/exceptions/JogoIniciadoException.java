package br.ufg.si.ad.commons.exceptions;
/**
 * @author bruno nogueira
 *
 * Excecao lancada para os jogadores que tentarem conectar em um tabuleiro onde o jogo ja esta acontecendo
 */
public class JogoIniciadoException extends JogoException {

	/**
	 *
	 */
	private static final long serialVersionUID = -9213850735767419473L;

	/**
	 * Construtor padrao da exception JogoIniciadoException
	 */
	public JogoIniciadoException() {
		super();
	}

	/**
	 * Construtor da exception JogoIniciadoException que recebe uma mensagem com a causa da excecao
	 *
	 * @param message contem a causa da excecao
	 */
	public JogoIniciadoException(String message) {
		super(message);
	}

	/**
	 * Construtor da exception JogoIniciadoException que encapsula uma excecao generica de JogoException
	 * @param e excecao generica de JogoException lancada pela aplicacao
	 */
	public JogoIniciadoException(JogoException e) {
		super(e);
	}
}
