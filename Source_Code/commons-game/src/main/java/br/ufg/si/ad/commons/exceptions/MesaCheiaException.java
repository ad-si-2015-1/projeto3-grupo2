package br.ufg.si.ad.commons.exceptions;
/**
 * @author bruno nogueira
 *
 *	Excecao lancada quando o jogador tenta conectar em um tabuleiro cuja mesa ja tem o numero maximo de participantes
 */
public class MesaCheiaException extends JogoException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Construtor padrao da Exception MesaCheiaException
	 */
	public MesaCheiaException() {
		super();
	}

	/**
	 * Construtor da exception MesaCheiaException que recebe uma mensagem com a causa da excecao
	 *
	 * @param message causa da excecao
	 */
	public MesaCheiaException(String message) {
		super(message);
	}

	/**
	 * Construtor da exception MesaCheiaException que encapsula uma excecao generica de JogoException
	 *
	 * @param e excecao generica de JogoException
	 */
	public MesaCheiaException(JogoException e) {
		super(e);
	}
}
