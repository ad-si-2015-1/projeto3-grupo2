package br.ufg.si.ad.commons.mensagens;

import java.io.Serializable;

import br.ufg.si.ad.commons.enums.Armas;
import br.ufg.si.ad.commons.enums.Locais;
import br.ufg.si.ad.commons.enums.Suspeitos;

/** @author Bruno Nogueira
 *
 * <p> Classe que contém os detalhes referente as mensagens trocadas no jogo para palpites e acusações*/
public class Mensagem implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7529398842296737627L;

	private Suspeitos suspeito;
	private Armas arma;
	private Locais local;

	/** Construtor da mensagem.
	 *
	 * <p>Para nao ocorrer corrupcao na mensagem so e possivel setar os atributos da Mensagem na construcao dessa */
	public Mensagem(Suspeitos supeito, Armas arma, Locais local) {
		super();
		this.suspeito = supeito;
		this.arma = arma;
		this.local = local;
	}

	/** Retorna o <b>SUSPEITO</b> dessa mensagem */
	public Suspeitos getSuspeito() {
		return suspeito;
	}

	/** Retorna a <b>ARMA</b> dessa mensagem */
	public Armas getArma() {
		return arma;
	}

	/** Retorna o <b>LOCAL</b> dessa mensagem*/
	public Locais getLocal() {
		return local;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arma == null) ? 0 : arma.hashCode());
		result = prime * result + ((local == null) ? 0 : local.hashCode());
		result = prime * result + ((suspeito == null) ? 0 : suspeito.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensagem other = (Mensagem) obj;
		if (arma != other.arma)
			return false;
		if (local != other.local)
			return false;
		if (suspeito != other.suspeito)
			return false;
		return true;
	}

}
