package br.ufg.si.ad.commons.modelo;

import java.io.Serializable;

import br.ufg.si.ad.commons.services.IServiceClient;

/** @author Bruno Nogueira
 *
 * Classe que contem os dados referente ao jogador;*/
public class Jogador implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -3404966246137538065L;
	private String nome;
	private IServiceClient cliente;

	/** Construtor base do Jogador */
	public Jogador() {
		// Construtor base
	}

	/** Construtor de Jogador que recebe o nome como parametro */
	public Jogador(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the cliente
	 */
	public IServiceClient getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(IServiceClient cliente) {
		this.cliente = cliente;
	}


}
