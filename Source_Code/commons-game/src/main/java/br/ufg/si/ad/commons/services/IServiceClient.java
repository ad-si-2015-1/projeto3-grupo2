package br.ufg.si.ad.commons.services;

import br.ufg.si.ad.commons.mensagens.Mensagem;
/**
 * @author bruno nogueira<br>
 *
 * <p>Interface do cliente que implementa metodos que podem ser chamados pelo servidor;
 * <p> Esses metodos vao permitir ao servidor maior controle sobre as acoes do lado cliente;
 *
 */
public interface IServiceClient{

	/**
	 * Metodo lancado pelo servidor para informar para o cliente que e a vez dele jogar;
	 */
	public void suaVez();

	/**
	 * Metodo lancado pelo servidor para informar para um cliente o palpite dado por outro cliente<br>
	 * @param mensagem contendo o palpite de outro jogador;
	 */
	public void palpiteOponente(String nomeJogador, Mensagem mensagem);

	/**
	 * Metodo utilizado pelo servidor para mandar informacoes diversas para o cliente;
	 *
	 * @param mensagem String contendo a mensagem que o servidor deseja passar;
	 */
	public void mensagemServidor(String mensagem);

}
