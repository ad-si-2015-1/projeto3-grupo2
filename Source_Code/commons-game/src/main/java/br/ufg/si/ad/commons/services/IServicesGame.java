package br.ufg.si.ad.commons.services;

import br.ufg.si.ad.commons.exceptions.JogoException;
import br.ufg.si.ad.commons.mensagens.Mensagem;
import br.ufg.si.ad.commons.modelo.Jogador;

/** @author bruno nogueira
 *
 * <p>Interface que mostra para o lado cliente/servidor quais os servicos conhecidos pela aplicacao<br>
 *
 * <p>O lado cliente faz a chamada dos servicos aqui presente.
 * <p>O lado do servidor implementa os servicos aqui presente.*/
public interface IServicesGame {

	/**
	 * Metodo responsavel por colocar um jogador para jogar o game
	 * @throws JogoException
	 * */
	public void iniciarJogador(Jogador jogador) throws JogoException ;

	/**
	 * Metodo responsavel por lancar um palpite de solucao do crime para o servidor
	 *
	 * @param mensagem que contem o palpite do jogador;
	 * @return String informando uma dica caso o palpite seja refrutado pelo servidor.<br>
	 * Se a String for NULA pode significar que o jogador acertou o palpite.
	 * */
	public String lancarPalpite(Mensagem mensagem) ;

	/**
	 * Metodo responssavel por lancar uma acusacao do crime para o servidor
	 *
	 * @param mensagem contendo a acusacao do jogador.
	 * @return Mensagem contendo a solucao do crime.<br>
	 * Se a solucao for diferente da mensagem, o jogador perdeu o jogo<br>
	 * Se a solucao for igual a mensagem do jogador, significa que ele ganhou o jogo
	 */
	public Mensagem lancarAcusacao(Mensagem mensagem) ;
}
