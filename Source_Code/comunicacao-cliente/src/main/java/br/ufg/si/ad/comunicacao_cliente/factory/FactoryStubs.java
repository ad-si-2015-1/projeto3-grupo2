package br.ufg.si.ad.comunicacao_cliente.factory;

import java.util.Properties;

import br.ufg.si.ad.comunicacao_cliente.implementacoes.IMPLEMENTACOES;
import br.ufg.si.ad.comunicacao_cliente.stubs.ILocalServer;
import br.ufg.si.ad.comunicacao_cliente.stubs.IRemoteCliente;

/**
 * @author bruno nogueira<br>
 *
 * <p>Essa classe é responsável por criar os Stubs do cliente e do servidor e inicia-los para funcionamento do lado cliente;
 *
 */
public class FactoryStubs {

	private static FactoryStubs factory;

	private ILocalServer localServer;
	private IRemoteCliente localClient;

	private FactoryStubs() {
		// Singleton (acho que tenho tesao nesse padrao)
	}

	public static FactoryStubs getFactory() {
		if(factory == null) {
			factory = new FactoryStubs();
		}
		return factory;
	}

	public ILocalServer getLocalServer() {

		if(localServer == null) {
			criarImplementacoes();
		}

		return localServer;
	}

	public IRemoteCliente getLocalClient() {

		if(localClient == null) {
			criarImplementacoes();
		}

		return localClient;
	}

	private void criarImplementacoes() {
		Properties prop = ManipuladorProperties.getProp();
		String imp = prop.getProperty("cliente.comunicacao.implementacao", "SOCKET");

		switch(IMPLEMENTACOES.valueOf(imp)) {
		case SOCKET:
			// Colocar aqui o projeto que implementa o socket;
			// localServer = Projeto.getImplementacao();
			// localCliente = Projeto.getImplementacao();
			break;

		default:
			break;

		}
	}

}
