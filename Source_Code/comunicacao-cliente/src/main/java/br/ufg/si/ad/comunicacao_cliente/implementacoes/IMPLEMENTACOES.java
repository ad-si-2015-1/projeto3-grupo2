package br.ufg.si.ad.comunicacao_cliente.implementacoes;

/**
 * @author bruno nogueira<br>
 *
 *<p> Enumeracao que mostra quais sao as implementacoes de rede possiveis para esse lado do jogo;
 */
public enum IMPLEMENTACOES {

	SOCKET("SOCKET"); //Retirar porque tem de ter alguma coisa implementada;

	private String nomeImplementacao;

	IMPLEMENTACOES(String nomeImplementacao) {
		this.nomeImplementacao = nomeImplementacao;
	}

	public String getNomeImplementacao() {
		return nomeImplementacao;
	}
}
