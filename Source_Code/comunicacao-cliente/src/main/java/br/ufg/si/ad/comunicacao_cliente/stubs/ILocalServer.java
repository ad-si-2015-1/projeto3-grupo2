package br.ufg.si.ad.comunicacao_cliente.stubs;

import br.ufg.si.ad.commons.services.IServicesGame;

/**
 * @author bruno nogueira<br>
 *
 *	<p>Interface local para fingir que e o servidor.
 *	<p>As implementacoes desse cara deve prover alguma forma de comunicacao remota para as chamada de metodos do servidor;
 *
 */
public interface ILocalServer extends IServicesGame {

}
