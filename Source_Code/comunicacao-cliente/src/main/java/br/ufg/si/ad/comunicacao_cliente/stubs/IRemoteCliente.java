package br.ufg.si.ad.comunicacao_cliente.stubs;

import br.ufg.si.ad.commons.services.IServiceClient;

/**
 * @author bruno nogueira<br>
 *
 *	<p>Interface local prover servicos remotos
 *	<p>As implementacoes desse cara deve prover alguma forma de comunicacao remota para permitir que os serviços remotos
 *possam chamar metodos do cliente que roda aqui;
 *
 */
public interface IRemoteCliente extends IServiceClient {

}
