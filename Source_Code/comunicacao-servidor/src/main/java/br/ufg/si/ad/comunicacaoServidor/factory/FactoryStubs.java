package br.ufg.si.ad.comunicacaoServidor.factory;

import java.util.Properties;

import br.ufg.si.ad.commons.services.IServicesGame;
import br.ufg.si.ad.comunicacao_servidor.factory.ManipuladorProperties;
import br.ufg.si.ad.servidor.negocio.Game;

/**
 * @author bruno nogueira<br>
 *
 * <p>Classe responsavel por intermediar a comunicacao da camada de implementacao com o servidor;
 */
public class FactoryStubs {

	private static FactoryStubs factory;

	/**
	 * Esse parametro guarda a instancia do Servidor em execucao;
	 */
	private IServicesGame instanciaGame;

	private FactoryStubs() {
		instanciaGame = Game.getGame();
		criarImplementacao();
	}

	public static FactoryStubs getFactory() {
		if(factory == null) {
			factory = new FactoryStubs();
		}
		return factory;
	}

	private void criarImplementacao() {
		Properties prop = ManipuladorProperties.getProp();
		String imp = prop.getProperty("servidor.comunicacao.implementacao", "SOCKET");

		switch(Implementacoes.valueOf(imp)) {
		case SOCKET:
			// cria a implementacao da comunicacao;
			break;

		default:
			break;

		}

	}

}
