package br.ufg.si.ad.comunicacaoServidor.factory;

/**
 * @author bruno nogueira<br>
 *
 *<p> Enumeracao que mostra quais sao as implementacoes de rede possiveis para esse lado do jogo;
 */
public enum Implementacoes {

	SOCKET("SOCKET"); //Retirar porque tem de ter alguma coisa implementada;

	private String nomeImplementacao;

	Implementacoes(String nomeImplementacao) {
		this.nomeImplementacao = nomeImplementacao;
	}

	public String getNomeImplementacao() {
		return nomeImplementacao;
	}
}
