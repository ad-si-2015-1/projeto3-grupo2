package br.ufg.si.ad.comunicacao_servidor.factory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ManipuladorProperties {

	public static Properties getProp() {
		Properties props = new Properties();
		FileInputStream file;
		try {
			file = new FileInputStream("/comunicacao.properties");
			props.load(file);
		} catch (FileNotFoundException e) {
			System.err.println("Arquivo nao encontrado");
		} catch (IOException e) {
			System.err.println("Problema carregar o arquivo");
		}

		return props;
	}

}
