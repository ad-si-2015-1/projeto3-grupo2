package br.ufg.si.ad.detetive.client.main;

import br.ufg.si.ad.commons.modelo.Jogador;
import br.ufg.si.ad.detetive.client.negocio.ClienteGame;
import br.ufg.si.ad.detetive.client.negocio.Visualizacoes;


/** @author Bruno Nogueira
 *
 * Classe principal do cliente que dispara as ações do cliente para jogar o jogo;*/
public class ClienteMain {

	private Jogador jogador;
	private ClienteGame jogo;

	/** Construtor da classe principal.<br>
	 *
	 * <p>Responsavel por iniciar o jogo */
	ClienteMain() {

		if(jogador == null && jogo == null) {
			jogador = new Jogador( Visualizacoes.nomearJogador() );
			jogo = new ClienteGame();

			new Thread( new InicializadorCliente( jogo ) ).start();
		}

	}

	public static void main(String[] args) {
		new ClienteMain();
	}

	/**
	 * @author bruno nogueira
	 *
	 * Classe interna que so tem um objetivo na vida: iniciar o jogo do cliente em uma Thread;
	 *
	 */
	class InicializadorCliente implements Runnable{

		private ClienteGame jogo;

		InicializadorCliente(ClienteGame jogo) {
			this.jogo = jogo;
		}

		public void run() {
			jogo.jogar();
		}


	}

}
