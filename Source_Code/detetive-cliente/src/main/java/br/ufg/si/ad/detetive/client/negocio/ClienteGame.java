package br.ufg.si.ad.detetive.client.negocio;

import br.ufg.si.ad.commons.mensagens.Mensagem;
import br.ufg.si.ad.commons.services.IServiceClient;
import br.ufg.si.ad.commons.services.IServicesGame;

/** @author bruno nogueira
 *
 * Classe responsavel por manter as regras de negocio do cliente.*/
public class ClienteGame implements IServiceClient{

	private IServicesGame comunicacao;

	private Boolean ativo = true;
	private Boolean minhaVez = false;

	/** Construtor padrao da classe ClienteGame */
	public ClienteGame() {

		comunicacao = criarComunicacao();

	}

	/** Metodo responsavel por pegar a implementacao de comunicacao que nao esta nesse projeto */
	private IServicesGame criarComunicacao() {
		IServicesGame com = null;
		/*
		 * TODO aqui deverá buscar uma implementação de comunicação.
		 */

		return com;
	}

	/** Metodo que contem a interacao do jogo */
	public void jogar() {

		int opcao;
		Mensagem mensagem;

		while(ativo) {

			if(minhaVez) {
				opcao = Visualizacoes.escolherAcao();

				if(opcao == 1) {

					mensagem = Visualizacoes.criarMensagem();

					// Metodo lançar palpite pode dar falha.
					// 1 - Nao eh hora de lancar palpite. Aguardar outro jogador
					// 2 - Ocorreu falha na rede. Lancar novamente;
					String dica = comunicacao.lancarPalpite(mensagem);
					if(!dica.isEmpty()) {
						Visualizacoes.mostrarDica(dica);
					} else {
						if(Visualizacoes.semDica()) {
							opcao = 2;
						}
					}

					minhaVez = false;
				}

				if(opcao == 2) {

					mensagem = Visualizacoes.criarMensagem();

					// Metodo lancar acusacao pode dar falha.
					// 1 - Nao eh hora de lancar acusacao. Aguardar outro jogador
					// 2 - Ocorreu falha na rede. Lancar novamente;
					Mensagem solucao = comunicacao.lancarAcusacao(mensagem);

					if(solucao.equals(mensagem)) {
						Visualizacoes.ganhou();
					} else {
						Visualizacoes.errou(solucao);
					}

					ativo = false;
					Thread.currentThread().interrupt(); // parar o cliente. Acabou o jogo
				}
			}

		}

	}

	public void suaVez() {
		minhaVez = true;
	}

	public void mensagemServidor(String mensagem) {
		Visualizacoes.mensagemPersonalizada(mensagem);
	}

	public void palpiteOponente(String nomeJogador, Mensagem mensagem) {
		Visualizacoes.mostrarPalpiteAdversario(nomeJogador, mensagem);
	}

}
