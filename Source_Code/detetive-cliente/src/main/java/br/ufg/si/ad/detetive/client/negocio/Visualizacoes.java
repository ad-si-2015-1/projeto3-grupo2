package br.ufg.si.ad.detetive.client.negocio;

import javax.swing.JOptionPane;

import br.ufg.si.ad.commons.enums.Armas;
import br.ufg.si.ad.commons.enums.Locais;
import br.ufg.si.ad.commons.enums.Suspeitos;
import br.ufg.si.ad.commons.mensagens.Mensagem;

public class Visualizacoes {

	public static String nomearJogador() {
		String nomeJogador = null;
		do {
			nomeJogador = JOptionPane.showInputDialog(null, "Digite o nome do jogador", "Jogador", JOptionPane.INFORMATION_MESSAGE);
		} while(nomeJogador == null || nomeJogador.replace(" ", "").length() == 0);

		return nomeJogador;
	}

	public static int escolherAcao() {
		String op;
		int valorOp = 0;

		StringBuilder sb = new StringBuilder();
		sb.append("Digite a opção desejada:\n");
		sb.append("Digite 1 para lançar palpite\n");
		sb.append("Digite 2 para lançar acusação\n");
		do {
			op = JOptionPane.showInputDialog(sb);
			try {
				valorOp = Integer.valueOf(op);
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Valor inválido! Digite um valor válido");
			}
		} while(valorOp < 1 || valorOp > 2);

		return valorOp;
	}

	public static void mostrarPalpiteAdversario(String nomeAdversario, Mensagem palpite) {
		JOptionPane.showMessageDialog(null, nomeAdversario + "diz: O criminoso " + palpite.getSuspeito() + " cometeu o crime com a arma " + palpite.getArma() + " no seguinte local: " + palpite.getLocal(), "Palpite dado", JOptionPane.INFORMATION_MESSAGE);
	}

	public static Mensagem criarMensagem() {

		JOptionPane.showMessageDialog(null, "Qual a sua suspeita?" );
		Mensagem palpite = new Mensagem(escolherSuspeito(), escolherArma(), escolherLocal());

		JOptionPane.showMessageDialog(null, "Palpite criado! O criminoso " + palpite.getSuspeito() + " cometeu o crime com a arma " + palpite.getArma() + " no seguinte local: " + palpite.getLocal());

		return palpite;

	}

	public static void mostrarDica(String dica) {
		JOptionPane.showMessageDialog(null, "Seu palpite está errado. Dica: " + dica, "Dica", JOptionPane.ERROR_MESSAGE);
	}

	public static boolean semDica() {
		int choose = JOptionPane.showConfirmDialog(null, "O servidor não conseguiu refrutar seu palpite. Provavelmente você acertou. Quer lançar acusação?", "Servidor não soube refrutar", JOptionPane.YES_NO_OPTION);
		if(choose == JOptionPane.YES_OPTION) {
			return true;
		} else {
			return false;
		}
	}

	public static void ganhou() {
		JOptionPane.showMessageDialog(null, "Você acertou e ganhou o jogo", "Vencedor", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void errou(Mensagem solucao) {
		JOptionPane.showMessageDialog(null, "Você errou! A solucao do crime é:  O criminoso " + solucao.getSuspeito() + " cometeu o crime com a arma " + solucao.getArma() + " no seguinte local: " + solucao.getLocal(), "Errou", JOptionPane.ERROR_MESSAGE);
	}

	public static void mensagemPersonalizada(String mensagem) {
		JOptionPane.showMessageDialog(null, mensagem, "Mensagem do servidor", JOptionPane.INFORMATION_MESSAGE);
	}

	private static Suspeitos escolherSuspeito() {
		StringBuilder sb = new StringBuilder();

		sb.append("Escolha um suspeito para acusar\n");
		sb.append("===============================\n");

		int i = 1;
		for(Suspeitos suspeito : Suspeitos.values()) {
			sb.append(i + ": " + suspeito.getNome() + "\n");
			i++;
		}

		boolean valido = false;
		int opcoes;

		do {
			opcoes = Integer.valueOf( JOptionPane.showInputDialog(sb) );
			if(opcoes < 1 || opcoes > Suspeitos.values().length) {
				JOptionPane.showMessageDialog(null, "Sua opção não é válida");
			} else {
				valido = true;
				return Suspeitos.getSuspeitoPorCodigo(opcoes);
			}
		} while(!valido);

		return null;
	}

	private static Armas escolherArma() {
		StringBuilder sb = new StringBuilder();

		sb.append("Escolha a arma que foi usada no crime\n");
		sb.append("=====================================\n");

		int i = 1;
		for(Armas arma : Armas.values()) {
			sb.append(i + ": " + arma.getNome() + "\n");
			i++;
		}

		boolean valido = false;
		int opcoes;

		do {
			opcoes = Integer.valueOf( JOptionPane.showInputDialog(sb) );
			if(opcoes < 1 || opcoes > Armas.values().length) {
				JOptionPane.showMessageDialog(null, "Sua opção não é valida");
			} else {
				valido = true;
				return Armas.getArmaPorCodigo(opcoes);
			}
		} while(!valido);

		return null;
	}

	private static Locais escolherLocal() {
		StringBuilder sb = new StringBuilder();

		sb.append("Escolha o local onde o crime ocorreu\n");
		sb.append("====================================\n");

		int i = 1;
		for(Locais local : Locais.values()) {
			sb.append(i + ": " + local.getNome() + "\n");
			i++;
		}

		boolean valido = false;
		int opcoes;

		do {
			opcoes = Integer.valueOf( JOptionPane.showInputDialog(sb) );
			if(opcoes < 1 || opcoes > Locais.values().length) {
				JOptionPane.showMessageDialog(null, "Sua opção não é válida");
			} else {
				valido = true;
				return Locais.getLocalPorCodigo(opcoes);
			}
		} while(!valido);

		return null;
	}
}
