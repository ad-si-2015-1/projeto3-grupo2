package br.ufg.si.ad.servidor.exception;

import br.ufg.si.ad.commons.exceptions.JogoException;

/**
 * @author bruno nogueira
 *
 * <p>Excecao lancada quando o jogador lanca um palpite errado para o servidor.<br>
 *
 * <p>Apesar de herdar de JogoException, essa excecao so e conhecida do lado do servidor;
 */
public class PalpiteErradoException extends JogoException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2282986154554035052L;

	/**
	 * Construtor padrao da exception PalpiteErradoException que recebe uma String contendo uma dica do erro;
	 * @param message Dica do erro
	 */
	public PalpiteErradoException(String message) {
		super(message);
	}

}
