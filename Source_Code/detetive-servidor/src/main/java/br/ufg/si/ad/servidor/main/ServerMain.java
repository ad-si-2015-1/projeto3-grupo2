package br.ufg.si.ad.servidor.main;

import br.ufg.si.ad.servidor.negocio.Game;

/**
 * @author bruno nogueira<br>
 *
 * <p> Classe que contem a inicializacao do servidor;
 *
 */
public class ServerMain {

	private Game jogo;

//	private List<Jogador> jogadores = new ArrayList<Jogador>();

	/**
	 * Construtor padrao da main do servidor;
	 */
	ServerMain() {

		jogo = Game.getGame();

		while(jogo.isServidorAtivo()) {
			// faz nada, só deixa o objeto vivo e disponivel para chamadas externas;
		}

//		testCriarJogadores();
//
//		testCriarJogadorAdicionalEValidarFalha();
//
//		String dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.ADVOGADO_SR_MARINHO, ARMAS.ADAGA, LOCAIS.BIBLIOTECA));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.ADVOGADO_SR_MARINHO, ARMAS.ADAGA, LOCAIS.BIBLIOTECA));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.DANCARIA_STA_ROSA, ARMAS.CORDA, LOCAIS.HALL));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.DANCARIA_STA_ROSA, ARMAS.CORDA, LOCAIS.HALL));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.DOUTORA_VIOLETA, ARMAS.ESCOPETA, LOCAIS.QUARTO));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.DOUTORA_VIOLETA, ARMAS.ESCOPETA, LOCAIS.QUARTO));
//		}
//
//
//
//
//		Mensagem mensagem = jogo.lancarAcusacao(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//
//		System.out.println(mensagem.getSuspeito().getNome() + " matou com " + mensagem.getArma().getNome() + " no " + mensagem.getLocal().getNome());
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.ADVOGADO_SR_MARINHO, ARMAS.ADAGA, LOCAIS.BIBLIOTECA));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.ADVOGADO_SR_MARINHO, ARMAS.ADAGA, LOCAIS.BIBLIOTECA));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.DANCARIA_STA_ROSA, ARMAS.CORDA, LOCAIS.HALL));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.DANCARIA_STA_ROSA, ARMAS.CORDA, LOCAIS.HALL));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.DOUTORA_VIOLETA, ARMAS.ESCOPETA, LOCAIS.QUARTO));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.DOUTORA_VIOLETA, ARMAS.ESCOPETA, LOCAIS.QUARTO));
//		}
//
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.ADVOGADO_SR_MARINHO, ARMAS.ADAGA, LOCAIS.BIBLIOTECA));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.ADVOGADO_SR_MARINHO, ARMAS.ADAGA, LOCAIS.BIBLIOTECA));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.CHEFE_DE_COZINHA_TONY, ARMAS.BOMBA, LOCAIS.COZINHA));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.DANCARIA_STA_ROSA, ARMAS.CORDA, LOCAIS.HALL));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.DANCARIA_STA_ROSA, ARMAS.CORDA, LOCAIS.HALL));
//		}
//
//
//
//		dica = jogo.lancarPalpite(new Mensagem(SUSPEITOS.DOUTORA_VIOLETA, ARMAS.ESCOPETA, LOCAIS.QUARTO));
//
//		if(dica != null) {
//			System.out.println(dica);
//		} else {
//			System.out.println("Você acertou danadao");
//			jogo.lancarAcusacao(new Mensagem(SUSPEITOS.DOUTORA_VIOLETA, ARMAS.ESCOPETA, LOCAIS.QUARTO));
//		}

	}

	public static void main(String[] args) {
		new ServerMain();
	}

//	private void testCriarJogadores() {
//
//		Jogador jogador;
//
//		for(int i = 1; i <= 6 ; i++) {
//			jogador = new Jogador("player" + i);
//			jogador.setCliente( new fakeImplServiceClient() );
//
//			try {
//				jogo.iniciarJogador(jogador);
//				jogadores.add(jogador);
//
//				System.out.println("Jogador " + jogador.getNome() + " adicionado");
//			} catch (JogoException e) {
//				System.err.println("Problema ao iniciar o jogador: " + e.getMessage());
//			}
//		}
//
//	}
//
//	private void testCriarJogadorAdicionalEValidarFalha() {
//		Jogador jogador = new Jogador("Sou problematico");
//
//		try {
//			jogo.iniciarJogador(jogador);
//		} catch (JogoException e) {
//			System.err.println("Nao fui inserido porque: " + e.getMessage());
//		}
//	}
//
//	/**
//	 * @author bruno
//	 *
//	 * Classe interna falsa para mentir que o jogador tem uma implementacao qualquer de service;
//	 */
//	class fakeImplServiceClient implements IServiceClient{
//
//		public void suaVez() throws RemoteException {
//			// faco nada
//		}
//
//		public void palpiteOponente(String nomeJogador, Mensagem mensagem) {
//			System.out.println(nomeJogador + " disse que " + mensagem.getSuspeito().getNome() + " matou com " + mensagem.getArma().getNome() + " no " + mensagem.getLocal().getNome());
//		}
//
//		public void mensagemServidor(String mensagem) throws RemoteException {
//			System.out.println(mensagem);
//		}
//
//	}


}
