package br.ufg.si.ad.servidor.negocio;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.ufg.si.ad.commons.enums.Armas;
import br.ufg.si.ad.commons.enums.Locais;
import br.ufg.si.ad.commons.enums.Suspeitos;
import br.ufg.si.ad.commons.exceptions.JogoException;
import br.ufg.si.ad.commons.exceptions.JogoIniciadoException;
import br.ufg.si.ad.commons.exceptions.MesaCheiaException;
import br.ufg.si.ad.commons.mensagens.Mensagem;
import br.ufg.si.ad.commons.modelo.Jogador;
import br.ufg.si.ad.commons.services.IServicesGame;
import br.ufg.si.ad.servidor.exception.PalpiteErradoException;
/**
 * @author bruno nogueira
 *
 * <p>Classe responsavel por implementar as regras de negocio do jogo
 */
public class Game implements IServicesGame {

	private static Game game;
	private List<Jogador> jogadores = new ArrayList<Jogador>();

	private Boolean jogoIniciado = false;
	private int indexJogadorRodada = -1; 	//Guarda qual a posição do jogador atual da lista. Começa com -1
											// Comeca com -1 para quando rodar o metodo getJogadorRodada pela primeira vez;

	private Mensagem solucaoCrime;

	/**
	 * Construtor privado de Game. Util para implementar o padrao Singleton
	 */
	private Game() {
		criarSolucaoCrime();
	}

	/**
	 * Metodo estatico que retorna a instancia corrente do jogo
	 * @return game Instancia corrente do jogo
	 */
	public static Game getGame() {
		if(game == null) {
			game = new Game();
		}
		return game;
	}

	public void iniciarJogador(Jogador jogador) throws JogoException {
		if(jogoIniciado) {
			throw new JogoIniciadoException("Jogo ja comecou. Impossivel adicionar jogador nessa mesa");
		} else if (jogadores.size() >= 6){
			throw new MesaCheiaException("Mesa ja esta cheia. Impossivel adicionar mais jogadores nessa mesa");
		} else {
			jogadores.add(jogador);
		}

		if(jogadores.size() == 6 && !jogoIniciado) {
			startarGame();
		}
	}

	public String lancarPalpite(Mensagem mensagem) {
		String dica = null;

		avisarTodosPalpite(mensagem);

		try {
			avaliarPalpite(mensagem);
		} catch (PalpiteErradoException e) {
			dica = e.getMessage();
			mandarMensagemTodos("Jogador " + jogadores.get(indexJogadorRodada).getNome() + " errou no palpite" );
			passarVez();
		}

		return dica;
	}

	public Mensagem lancarAcusacao(Mensagem mensagem) {

		avisarTodosPalpite(mensagem);

		try {
			avaliarPalpite(mensagem);
			mandarMensagemTodos("Jogador " + jogadores.get(indexJogadorRodada).getNome() + " acertou na acusacao e ganhou o jogo");
		} catch (PalpiteErradoException e) {
			System.err.println("Jogador errou na acusacao. Eliminar jogador");
			mandarMensagemTodos("Jogador " + jogadores.get(indexJogadorRodada).getNome() + "foi eliminado por errar a acusacao");
			jogadores.remove(indexJogadorRodada);
			passarVez();
		}

		game = null; // Termina o jogo atual para começar um novo;

		return solucaoCrime;
	}

	public Boolean isServidorAtivo() {
		game = getGame();
		return this.game != null ? true : false;
	}

	private void criarSolucaoCrime() {

		if(solucaoCrime != null) {
			System.out.println("Esse crime já possui uma solução. Não criarei outra");
			return;
		}

		Random randomico = new Random();

		int criminoso = (randomico.nextInt( Suspeitos.values().length )) + 1;
		int local = (randomico.nextInt( Locais.values().length )) + 1;
		int arma = (randomico.nextInt( Armas.values().length )) + 1;

		Suspeitos criminosoDefinido = Suspeitos.getSuspeitoPorCodigo(criminoso);
		Locais localDefinido = Locais.getLocalPorCodigo(local);
		Armas armaDefinida = Armas.getArmaPorCodigo(arma);


		solucaoCrime = new Mensagem(criminosoDefinido, armaDefinida, localDefinido);

	}

	private void avisarTodosPalpite(Mensagem palpite) {
		for(Jogador jogador : jogadores) {

			try {
				if( !jogador.equals( jogadores.get(indexJogadorRodada) ) ) {
					jogador.getCliente().palpiteOponente(jogadores.get(indexJogadorRodada).getNome(), palpite);
				}
			} catch (Exception e) {
				// se deu problema nao faz nada e segue em frente;
			}

		}
	}

	private void mandarMensagemTodos(String mensagem) {
		for(Jogador jogador : jogadores) {

			try {
				if( !jogador.equals( jogadores.get(indexJogadorRodada) ) ) {
					jogador.getCliente().mensagemServidor(mensagem);
				}
			} catch (Exception e) {
				// se deu problema nao faz nada e segue em frente;
			}

		}
	}

	private void passarVez() {
		Jogador jogador = getProximoJogador();

		try {
			jogador.getCliente().suaVez();
		} catch (Exception e) {
			// Tem o que fazer nao. Se deu problema passa para o proximo
			passarVez();
		}
	}

	private Jogador getProximoJogador() {

		indexJogadorRodada++;

		if(indexJogadorRodada == jogadores.size()) {
			indexJogadorRodada = 0;
		}

		return jogadores.get(indexJogadorRodada);
	}

	private void avaliarPalpite(Mensagem palpite) throws PalpiteErradoException {
		List<String> erros = new ArrayList<String>();

		if( palpite.getArma() != solucaoCrime.getArma() ) {
			erros.add( palpite.getArma().getNome() );
		}
		if( palpite.getLocal() != solucaoCrime.getLocal() ) {
			erros.add( palpite.getLocal().getNome() );
		}
		if( palpite.getSuspeito() != solucaoCrime.getSuspeito() ) {
			erros.add( palpite.getSuspeito().getNome() );
		}

		if(erros.size() != 0) {
			throw new PalpiteErradoException( erros.get( new Random().nextInt( erros.size() ) ) );
		}

	}

	private void startarGame() {
		jogoIniciado = true;
		passarVez();
	}

}
